$(window).load(function() {
    $(".khungAnhCrop img").each(function() {
            $(this).removeClass("wide tall").addClass((this.width / this.height > $(this).parent().width() / $(this).parent().height())
                    ? "wide"
                    : "tall");
        });
});

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }    
}

function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}



$(document).ready(function(){
  $('.slider').slick({
    slidesToShow: 1,
  	slidesToScroll: 1,
  	autoplay: true,
  	autoplaySpeed: 2800,
  	arrows: false,
  });
  $('.left').click(function(){
  	$('.slider').slick('slickPrev');
  })
  $('.right').click(function(){
  	$('.slider').slick('slickNext');
  })	

  $('.slider1').slick({
    slidesToShow: 4,
  	slidesToScroll: 1,
  	autoplay: true,
  	autoplaySpeed: 3000,
  	arrows: false,
  	responsive: [
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,                
            }
        },

         {
             breakpoint: 992,
             settings: {
                 slidesToShow: 3,
             }
         },
  	]    
  });
  $('.left1').click(function(){
  	$('.slider1').slick('slickPrev');
  })
  $('.right1').click(function(){
  	$('.slider1').slick('slickNext');
  })	

  $('.slider2').slick({
    slidesToShow: 1,
  	slidesToScroll: 1,  	
  	arrows: false,
  	dots:true,
  	customPaging : function(slider, i) {
	var thumb = $(slider.$slides[i]).data();
	return '<a>'+(i+1)+'</a>';
	},
  });

  $('.slider3').slick({
      slidesToShow: 2,
      slidesToScroll: 2,
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: false,
  });
  $('.left1').click(function () {
      $('.slider3').slick('slickPrev');
  })
  $('.right1').click(function () {
      $('.slider3').slick('slickNext');
  })
});


$('.parallax-product').parallax({ imageSrc: 'images/homepage/anh.jpg' });
$('.parallax-footer').parallax({ imageSrc: 'images/background/footer.jpg' });
$('.parallax-about').parallax({ imageSrc: 'images/about/banner.jpg' });
$('.parallax-pd').parallax({ imageSrc: 'images/products/banner1.jpg' });
$('.parallax-baking').parallax({ imageSrc: 'images/bakingclass/banner.jpg' });
$('.parallax-gallery').parallax({ imageSrc: 'images/gallery/banner.jpg' });
$('.parallax-contact').parallax({ imageSrc: 'images/contact/banner.jpg' });
$('.parallax-cart').parallax({ imageSrc: 'images/cart/banner.jpg' });

function iconbar1() {
    document.getElementById("baricon").classList.toggle("show");
}

$(window).resize(function () {
    var width = $(window).width();
    if (width <= 768) {
        function scrollFunction() {
            if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                document.getElementById("myBtn").style.display = "block";
            } else {
                document.getElementById("myBtn").style.display = "none";
            }
        }
    }
    
   

});

function cart(i){
    document.getElementById(i).style.visibility = 'visible';    
    setTimeout(function () {        
        $("#add").fadeIn(200).delay(1200).fadeOut(200);
        $("#add1").fadeIn(200).delay(1200).fadeOut(200);
        $("#add2").fadeIn(200).delay(1200).fadeOut(200);
        $("#add3").fadeIn(200).delay(1200).fadeOut(200);
        $("#add4").fadeIn(200).delay(1200).fadeOut(200);
        $("#add5").fadeIn(200).delay(1200).fadeOut(200);
        $("#add6").fadeIn(200).delay(1200).fadeOut(200);
        $("#add7").fadeIn(200).delay(1200).fadeOut(200);
        $("#add8").fadeIn(200).delay(1200).fadeOut(200);
        $("#add9").fadeIn(200).delay(1200).fadeOut(200);
        $("#add10").fadeIn(200).delay(1200).fadeOut(200);
        $("#add11").fadeIn(200).delay(1200).fadeOut(200);        
    });
    setTimeout(function () {
        document.getElementById("number").innerHTML = '1';
        document.getElementById("number1").innerHTML = '1';
        document.getElementById("number2").innerHTML = '1';
        document.getElementById("number3").innerHTML = '1';
        document.getElementById("number4").innerHTML = '1';
        document.getElementById("number5").innerHTML = '1';
        document.getElementById("number6").innerHTML = '1';
        document.getElementById("number7").innerHTML = '1';
        document.getElementById("number8").innerHTML = '1';
        document.getElementById("number9").innerHTML = '1';
        document.getElementById("number10").innerHTML = '1';
        document.getElementById("number11").innerHTML = '1';
    }, 1200);    
}



var value = 1;
var value1 = 1;
var value2 = 1;
var value3 = 1;
var value4 = 1;
var value5 = 1;
var value6 = 1;
var value7 = 1;
var value8 = 1;
var value9 = 1;
var value10 = 1;
var value11 = 1;

function plus() {    
    value++;
    document.getElementById("number").innerHTML = value;    
}

function minus() {
    if (value > 1) {
        value--;
        document.getElementById("number").innerHTML = value;
    }
    else
        document.getElementById("number").innerHTML = '1';
}

function plus1() {
    value1++;
    document.getElementById("number1").innerHTML = value1;
}

function minus1() {
    if (value1 > 1) {
        value1--;
        document.getElementById("number1").innerHTML = value1;
    }
    else
        document.getElementById("number1").innerHTML = '1';
}

function plus2() {
    value2++;
    document.getElementById("number2").innerHTML = value2;
}

function minus2() {
    if (value2 > 1) {
        value2--;
        document.getElementById("number2").innerHTML = value2;
    }
    else
        document.getElementById("number2").innerHTML = '1';
}

function plus3() {
    value3++;
    document.getElementById("number3").innerHTML = value3;
}

function minus3() {
    if (value3 > 1) {
        value3--;
        document.getElementById("number3").innerHTML = value3;
    }
    else
        document.getElementById("number3").innerHTML = '1';
}

function plus4() {
    value4++;
    document.getElementById("number4").innerHTML = value4;
}

function minus4() {
    if (value4 > 1) {
        value4--;
        document.getElementById("number4").innerHTML = value4;
    }
    else
        document.getElementById("number4").innerHTML = '1';
}

function plus5() {
    value5++;
    document.getElementById("number5").innerHTML = value5;
}

function minus5() {
    if (value5 > 1) {
        value5--;
        document.getElementById("number5").innerHTML = value5;
    }
    else
        document.getElementById("number5").innerHTML = '1';
}

function plus6() {
    value6++;
    document.getElementById("number6").innerHTML = value6;
}

function minus6() {
    if (value6 > 1) {
        value6--;
        document.getElementById("number6").innerHTML = value6;
    }
    else
        document.getElementById("number6").innerHTML = '1';
}

function plus7() {
    value7++;
    document.getElementById("number7").innerHTML = value7;
}

function minus7() {
    if (value7 > 1) {
        value7--;
        document.getElementById("number7").innerHTML = value7;
    }
    else
        document.getElementById("number7").innerHTML = '1';
}

function plus8() {
    value8++;
    document.getElementById("number8").innerHTML = value8;
}

function minus8() {
    if (value8 > 1) {
        value8--;
        document.getElementById("number8").innerHTML = value8;
    }
    else
        document.getElementById("number8").innerHTML = '1';
}

function plus9() {
    value9++;
    document.getElementById("number9").innerHTML = value9;
}

function minus9() {
    if (value9 > 1) {
        value9--;
        document.getElementById("number9").innerHTML = value9;
    }
    else
        document.getElementById("number9").innerHTML = '1';
}

function plus10() {
    value10++;
    document.getElementById("number10").innerHTML = value10;
}

function minus10() {
    if (value10 > 1) {
        value10--;
        document.getElementById("number10").innerHTML = value10;
    }
    else
        document.getElementById("number10").innerHTML = '1';
}

function plus11() {
    value11++;
    document.getElementById("number11").innerHTML = value11;
}

function minus11() {
    if (value11 > 1) {
        value11--;
        document.getElementById("number11").innerHTML = value11;
    }
    else
        document.getElementById("number11").innerHTML = '1';
}



function buy() {
    $("#cartbox").addClass("payment");
    setTimeout(function () {
        $("#cartbox").removeClass("payment");
    },1500);
}

function clsdetail() {
    $("#bangct").addClass("bangdetail");
}

function tat() {
    $("#bangct").removeClass("bangdetail");
}

function clsdetail1() {
    $("#bangct1").addClass("bangdetail");
}

function tat1() {
    $("#bangct1").removeClass("bangdetail");
}

function clsdetail2() {
    $("#bangct2").addClass("bangdetail");
}

function tat2() {
    $("#bangct2").removeClass("bangdetail");
}




